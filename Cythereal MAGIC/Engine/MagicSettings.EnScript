/* Cythereal MAGIC related settings.
Example usage:
    // Create local class for holding the settings values.
    MagicSettingsClass MagicSettings();
	// Create controller class for updating and storing settings.
	MagicSettingsControllerClass MagicSettingsController(MagicSettings);
	
*/

/* Class to hold all settings values */
class MagicSettingsClass {

  String ApiKey;
  // Storing base is necessary because of weirdness with the C# DLL.
  // EnScript doesn't support default parameters and the auto-generated C# DLL
  // uses default parameters to set api base. The workaround is to always pass
  // the base.
  String ApiBase;
  String Title;
  double SimilarityThreshold;
  CaseClass Case;
  BookmarkClass CategoriesBookmarkRoot;
  BookmarkClass CorrelationsBookmarkRoot;

  MagicSettingsClass(CaseClass myCase, const String & myKey = "", const String & myBase = "https://api.magic.cythereal.com/", const double myThreshold = 0.7 ) {
    Case = myCase;
    ApiKey = myKey;
    ApiBase = myBase;
    SimilarityThreshold = myThreshold;
    Title = "Cythereal MAGIC";
    CategoriesBookmarkRoot = GetOrCreateBookmarkFolder("Cythereal Magic Categories");
    CorrelationsBookmarkRoot = GetOrCreateBookmarkFolder("Cythereal Magic Correlations");
  }

  BookmarkClass GetOrCreateBookmarkFolder(const String & folderName) {
    if (BookmarkClass folder = Case.BookmarkRoot().Find(folderName)) {
      return folder;
    }
    else {
      return new BookmarkClass(Case.BookmarkRoot(), folderName, NodeClass::FOLDER);
    }
  }

}

// Class for setting and updating the settings class.
class MagicSettingsControllerClass {

  MagicSettingsClass MySettings;

  MagicSettingsControllerClass(MagicSettingsClass mySettings):
    MySettings = mySettings
  {

    // And then read from storage
    ReadSettings();
  }

  void ReadSettings(){
    UpdateStorage(0);
  }

  void SaveSettings() {
    UpdateStorage(StorageClass::WRITE);
  }

  String CheckName(String name){
    if(name.SubString(name.GetLength()-1, 1) == " ")
      name = name.SubString(0, name.GetLength()-1);
    return name;
  }

  private void UpdateStorage(uint options) {
   StorageClass storage("Cythereal MAGIC v0.0", options);
   storage.Value("ApiKey", MySettings.ApiKey);
   storage.Value("ApiBase", MySettings.ApiBase);
   storage.Value("SimilarityThreshold", MySettings.SimilarityThreshold);
  }

  void UpdateSettings() {
    MagicSettingsDialogClass diag(null, MySettings);
      if (diag.Execute() == SystemClass::OK) {
	      MySettings.ApiKey = MySettings.ApiKey;
        MySettings.ApiBase = MySettings.ApiBase;
        MySettings.SimilarityThreshold = MySettings.SimilarityThreshold;
        SaveSettings();
      }
  }

}

/* DialogClass for updating settings. */
class MagicSettingsDialogClass: DialogClass {

  StringEditClass ApiKey;
  StringEditClass ApiBase;
  DoubleEditClass SimilarityThreshold;


  MagicSettingsDialogClass(DialogClass parent, MagicSettingsClass settings):
    DialogClass(parent, "Cythereal MAGIC Configuration"),
    ApiBase(this, "API URL", START, START, 200, 10, 0, settings.ApiBase, 100, WindowClass::EditOptions::REQUIRED),
    ApiKey(this, "Api Key", START, START+30, 200, 10, 0, settings.ApiKey, 100, WindowClass::EditOptions::REQUIRED),
    SimilarityThreshold(this, "Similarity Threshold", START, START+30*2, 200, 10, 0, settings.SimilarityThreshold, 0, 1, WindowClass::EditOptions::REQUIRED)
  {  }

}
