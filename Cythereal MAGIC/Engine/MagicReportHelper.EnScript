// Utility classes for interacting with and displaying MAGIC Report data.

/********************************/
/** Magic Correlations  **/
/********************************/

class MagicReportHelperClass {

  HandlerClass MagicCorrelationHandler() {
    // Create the handler
    HandlerClass hand(null, "Handler", 0, typeof(MagicCorrelation));
    new HandlerClass::StringFieldClass(hand, "Matched Binary", 0, MagicCorrelation::property(match_id), "_match");
    new HandlerClass::StringFieldClass(hand, "Similarity", 0, MagicCorrelation::property(similarity), "_similarity");
    new HandlerClass::StringFieldClass(hand, "Additional Notes", 0, MagicCorrelation::property(notes), "_notes");
    return hand;
  }

  /********************************/

  HandlerClass MagicCategoriesHandler() {
    // Create the handler
    HandlerClass hand(null, "Handler", 0, typeof(MagicCategory));
    new HandlerClass::StringFieldClass(hand, "Category", 0, MagicCategory::property(category_name), "_name");
    new HandlerClass::StringFieldClass(hand, "Score", 0, MagicCategory::property(category_score), "_score");
    return hand;
  }

  /********************************/

}

/********************************/

class MagicCorrelation: NameListClass {
  property String match_id,
                  similarity,
                  notes;

  MagicCorrelation(MagicCorrelation parent=null, const String & match_id="", const String & similarity="", const String & notes=""):
    NameListClass(parent, match_id),
    match_id = match_id,
    similarity = similarity,
    notes = notes
  {
  }

}

/********************************/

class MagicCategory: NameListClass {
  property String category_name,
                  category_score;

  MagicCategory(MagicCategory parent=null, const String & category_name="", const String & category_score=0):
    NameListClass(parent, category_name),
    category_name=category_name,
    category_score=category_score
  {
  }

}

/********************************/