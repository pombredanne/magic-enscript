
assembly embed "Include\CytherealMagic.dll"

include "MagicReportHelper"
include "EnCaseHelper"

// The values in this Enum MUST match
// the values in the C# magic client.
// This replication exists because I (CAL) can't
// figure out how to access the value of the enum
// directly from C#. I have asked a question in the support
// forums to no avail.
// https://support.guidancesoftware.com/forums/index.php?threads/c-dot-net-enum-in-enscript.45386/

class MagicMatchType {
  enum MagicMatchTypeEnum {

    SimilarPackerSimilarPayload = 0;
    SimilarPackerDifferentPayload = 1;
    DifferentPackerSimilarPayload = 2;
    WeakSimilar = 3;

  }
}


class MagicControllerClass: NodeClass {

  property String Title;
  MagicSettingsClass MagicSettings;
  EnCaseHelperClass EnCaseHelper;
  MagicReportHelperClass MagicReportHelper;
  CytherealMagic::Api::MagicReportApi report_client;
  CytherealMagic::Api::FileOperationsApi file_client;

  MagicControllerClass(MagicSettingsClass mySettings)
  {
    MagicSettings = mySettings;
    MagicReportHelper = new MagicReportHelperClass();
    Title = "Cythereal MAGIC";
    report_client = new CytherealMagic::Api::MagicReportApi(MagicSettings.ApiBase);
    file_client = new CytherealMagic::Api::FileOperationsApi(MagicSettings.ApiBase);
  }


  public bool GetCorrelationsByHash() {

    EntryFileClass ef;
    ef = GetSelectedFile();

    if (!ef) {
      Console.WriteLine("No file found");
      return false;
    }

    // Get the hash of the file.
    SHA1Class hash();
    ef.Seek(0);
    ef.ComputeSHA1(hash);
    Console.WriteLine("Get MAGIC Correlations. FileName = {0} ; SHA1 = {1}", ef.Name(), hash);

    // If we have a hash, get MAGIC for it.
    if (hash.IsValid()) {

      CytherealMagic::Model::MagicMatchesResponse response = report_client.MagicCorrelations(MagicSettings.ApiKey, hash.GetString(),
                                                                                             null,
                                                                                             null);

      if (response.Statuscode().ToString() != "0") {
        String msg;
        msg += "Error retrieving correlations\n";
        msg += "Please contact support@cythereal.com with the following information\n\n";
        msg += "SHA1: " + hash + "\n";
        msg += "Status Code: " + response.Statuscode().ToString() + "\n";
        msg += "Error Message: " + response.Message();
        SystemClass::Message(SystemClass::ICONSTOP, "Error", msg);
        return false;
      }

      // Create bookmark folders to hold the results.
      // Top level folder for this hash.
      BookmarkClass bmarkRoot(MagicSettings.CorrelationsBookmarkRoot, hash, NodeClass::FOLDER);
      bmarkRoot.SetComment("Matches for SHA1 " + hash);
      // Folder for malware with similar packer.
      BookmarkDataClass similarPackerBmark(bmarkRoot, "Similar Packer");
      similarPackerBmark.SetComment("Binaries that have a packer similar to " + hash);
      // Folder for malware with similar payload.
      BookmarkDataClass similarPayloadBmark(bmarkRoot, "Similar Payload");
      similarPayloadBmark.SetComment("Binaries that have a similar payload to " + hash);

      // Create root data nodes to hold all correlations.
      MagicCorrelation similarPackerRoot();
      MagicCorrelation similarPayloadRoot();

      // Get the matches object.
      CytherealMagic::Model::MagicMatches matches = response.Answer();

      Console.WriteLine(matches.ToJson());

      // Iterate over the list of matches.
      foreach (CytherealMagic::Model::MagicMatchesDetails details in matches.Details()) {
        String matchId = details.Sha1();
        String similarity = details.MaxSimilarity().ToString();

        if (details.MatchType().ToString() == MagicMatchType::SimilarPackerSimilarPayload) {
          new MagicCorrelation(similarPackerRoot, matchId, similarity, "Also has similar payload");
          new MagicCorrelation(similarPayloadRoot, matchId, similarity, "Also has similar packer");
        }
        else if (details.MatchType().ToString() == MagicMatchType::SimilarPackerDifferentPayload) {
          new MagicCorrelation(similarPackerRoot, matchId, similarity);
        }
        else if (details.MatchType().ToString() == MagicMatchType::DifferentPackerSimilarPayload) {
          new MagicCorrelation(similarPayloadRoot, matchId, similarity);
        }
        // Ignoring weak similar because unsure how to interpret.
        else if (details.MatchType().ToString() == MagicMatchType::WeakSimilar) {
          Console.WriteLine("Skipping weak similar SHA1: " + matchId);
        }

      }


      // Attach all correlations to folder.
      if (similarPackerBmark.SetRoot(similarPackerRoot, MagicReportHelper.MagicCorrelationHandler()) & similarPayloadBmark.SetRoot(similarPayloadRoot, MagicReportHelper.MagicCorrelationHandler())) {
        String msg;
        msg += "Correlations for file with SHA1 " + hash;
        msg += " stored in bookmark folder \"Cythereal Magic Correlations/" + hash + "\"";
        msg += "\n";
        Console.WriteLine(msg);
        SystemClass::Message(SystemClass::ICONINFORMATION, "Correlations Retrieved for " + hash, msg);
        return true;

      } else {
        String msg;
        msg += "Error creating bookmarks for file with SHA1 " + hash;
        msg += "\n Attempted to store in bookmark folder \"Cythereal Magic Correlations/" + hash + "\"";
        msg += "\n";
        Console.WriteLine(msg);
        SystemClass::Message(SystemClass::ICONSTOP, "Error Retrieving Correlations for " + hash, msg);
        return false;
      }

    }
    return false;
  }

public bool GetCategoriesByHash() {
    EntryFileClass ef;
    ef = GetSelectedFile();

    if (!ef) {
      Console.WriteLine("No file found");
      return false;
    }

    // Get the hash of the file.
    SHA1Class hash();
    ef.Seek(0);
    ef.ComputeSHA1(hash);
    Console.WriteLine("Get MAGIC Categories. FileName = {0} ; SHA1 = {1}", ef.Name(), hash);

    // If we have a hash, get MAGIC for it.
    if (hash.IsValid()) {
      CytherealMagic::Model::MagicCategoriesResponse response = report_client.MagicCategories(MagicSettings.ApiKey, hash);

      if (response.Statuscode().ToString() != "0") {
        String msg;
        msg += "Error retrieving categories\n";
        msg += "Please contact support@cythereal.com with the following information\n\n";
        msg += "SHA1: " + hash + "\n";
        msg += "Status Code: " + response.Statuscode().ToString() + "\n";
        msg += "Error Message: " + response.Message();
        SystemClass::Message(SystemClass::ICONSTOP, "Error", msg);
        return false;
      }

      CytherealMagic::Model::MagicCategories categories = response.Answer();

      Console.WriteLine(categories.ToJson());

      // Check if we have any categories
      if (categories.CategorizationResult().TotalCategories().ToString() == 0 && categories.GroundTruth().TotalCategories().ToString() == 0) {
        String msg = "No categories for hash " + hash;
        SystemClass::Message(SystemClass::ICONINFORMATION, "No Categories Found", msg);
        return true;
      }

      // Create bookmark folders to hold the results.
      // Top level folder for this hash.
      BookmarkClass bmarkRoot(MagicSettings.CategoriesBookmarkRoot, hash, NodeClass::FOLDER);
      bmarkRoot.SetComment("Categories for SHA1 " + hash);
      // Folder for ground truth.
      BookmarkDataClass groundTruthBmark(bmarkRoot, "Ground Truth Categories");
      groundTruthBmark.SetComment("Categories manually added to binary " + hash);
      // Folder for computes categories.
      BookmarkDataClass categorizationBmark(bmarkRoot, "Categorization Result");
      categorizationBmark.SetComment("Result of categorizing binary " + hash);

      // Create root data nodes to hold all categories.
      MagicCategory groundTruthRoot();
      MagicCategory categorizationRoot();

      // Add ground truth categories.
      foreach (CytherealMagic::Model::MagicCategoriesGroundTruthCategories category in categories.GroundTruth().Categories()) {
        new MagicCategory(groundTruthRoot, category.Name(), category.Score().ToString());
      }
      // Add categorization results.
      foreach (CytherealMagic::Model::MagicCategoriesCategorizationResultCategories category in categories.CategorizationResult().Categories()) {
        new MagicCategory(categorizationRoot, category.Name(), category.Score().ToString());
      }

      // Attach all correlations to folder.
      if (groundTruthBmark.SetRoot(groundTruthRoot, MagicReportHelper.MagicCategoriesHandler()) & categorizationBmark.SetRoot(categorizationRoot, MagicReportHelper.MagicCategoriesHandler())) {
        String msg;
        msg += "Categeories for file with SHA1 " + hash;
        msg += " stored in bookmark folder \"Cytheral Magic Categories/" + hash + "\"";
        msg += "\n";
        Console.WriteLine(msg);
        SystemClass::Message(SystemClass::ICONINFORMATION, "Categories Retrieved for " + hash, msg);
        return true;
      }

    }
    return false;
  }


  public bool SubmitFile() {

    EntryFileClass ef;
    ef = GetSelectedFile();

    if (!ef) {
      Console.WriteLine("No file found");
      return false;
    }

    Console.WriteLine("FileName = {0}", ef.Name());

    // Have to pass stream using DotNetStreamClass
    System::IO::Stream inputStream  = new DotNetStreamClass(ef);
    // Have to provide all options because EnScript doesn't respect default parameters.
    CytherealMagic::Model::ResponseObject response = file_client.Upload(MagicSettings.ApiKey, inputStream, ef.Name(), "", null, null);
    inputStream.Dispose();

    String msg;
    msg += "Upload result for file " + ef.Name() + "\n\n";
    msg += response.ToJson();

    String title = "Upload Result";
    SystemClass::Message(GetIcon(100), title, msg);
    Console.WriteLine(title);
    Console.WriteLine(msg);

    return true;

  }


  /* Return the currently selected file in the case.
  Gives an error message and returns null if no file selected or multiple files selected
  */
  private EntryFileClass GetSelectedFile() {

    EntryFileClass ef();  // Used to get the file from the EntryClass.

    // The previously selected case.
    CaseClass c;
    c = MagicSettings.Case;

    // Get the current entry.
    // e will either be an entry or null after this.
    long offset, size;
    EntryClass e;
    e = EnCaseHelper.GetCurrentEntry(c, offset, size);

    // If we have an entry, return the entry file.
    // Else return null
    if (e) {
      ef.Open(e);
      if (ef) {
        return ef;
      } else {
        String msg;
        msg += "File not Found.\n";
        msg += "Is a file selected?";
        SystemClass::SetLastError(msg);
        SystemClass::Message(SystemClass::ICONSTOP, "Error", msg);
      }
    }
    return null;

  }

  private SystemClass::MessageOptions GetIcon(int score) {
    if (score >= 75)
      return SystemClass::ICONINFORMATION;
    else if ( (score < 75) && (score>=25) )
      return SystemClass::ICONQUESTION;
    else if ( (score < 25) && (score>=0) )
      return SystemClass::ICONSTOP;

    return SystemClass::ICONQUESTION;
  }

}