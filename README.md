# Cythereal MAGIC Encase Plugin

This is the EnScript source files for the Cythereal MAGIC plugin.

This plugin relies on a C# client contained at: https://bitbucket.org/cythereal/magic-clients
Pre-compiled DLLs are included in this repository.

Cythereal MAGIC extracts intelligence from malicious programs (malware) to aid in cyber defense and incidence response. It combines two orthogonal areas � semantic inference and statistical inference � along with big data analytics to create what may be termed as �Google for malware�. Using a patent-pending technology, Cythereal MAGIC can make connections between malware, peering through its protection mechanisms to discover intrinsic relationships that provide strong evidence for classification and attribution.

This plugin enables interacting with the [MAGIC API](docs.cythereal.com) directly from within EnCase.

* MAGIC User Documentation: http://docs.cythereal.com
* MAGIC API Reference: https://api.magic.cythereal.com/docs
* Cythereal Homepage: http://cythereal.com

## Supported Operations

* [Upload highlighted file][MAGIC Upload] to MAGIC for analysis.
* Retrieving the [MAGIC Correlations report][MAGIC Correlations].


# Using the plugin

Requires .NET 4.6.1+

Use of the Cythereal MAGIC service requires an API. If you don't have a key already, please [request a key](http://docs.cythereal.com/en/latest/src/accessing_api.html#request-api-key) by emailing support@cythereal.com with the subject �Key Request�. Include your name and email address you want the key to be registered under.

Load the plugin by running the script "Cythereal MAGIC/Cythereal MAGIC.enscript" in EnCase. (Double click the file or load the EnScript file using the "EnScript" menu in EnCase.)

The MAGIC plugin creates three new menu entries:

* **Cythereal MAGIC Settings**: Configure the MAGIC plugin.
* **Query MAGIC Correlations**: Get the [MAGIC correlations][MAGIC Correlations] for a highlighted file.
  The MAGIC correlations report will be displayed in a new message window. Only a single file can be highlighted at one time.
* **Query MAGIC Categories**: Get the [MAGIC categories][MAGIC Categories] for a highlighted file.
* **Submit to Cythereal MAGIC**: Submit a highlighted file to the MAGIC service.
  The file will be submitted and analyzed by MAGIC. After analysis is complete, the MAGIC correlations can be retrieved.

If this is your first time using the plugin, add your API key using the **Cythereal MAGIC Settings** menu.

The other menu options require a case to be open and a single file to be highlighted.

# Plugin Source

The EnScript entry point is "Cythereal MAGIC/Cythereal MAGIC.enscript". This script is responsible for creating the UI elements and connecting these elements to their functionality. All functionality is contained within EnScripts in the "Cythereal MAGIC/Engine" directory.

* **JsonHelper.EnScript**: Contains helper functions for interacting with JSON strings.
* **MagicSettings.EnScript**: Holds the configurable settings for the MAGIC plugin.
* **MagicController.EnScript**: Main script for interacting with the MAGIC API. All calls to the API go through this script. This script uses the MagicClient script to make the HTTP query to the API, parses the returned response, and returns this parsed response.
* **Include/**: Contains the dot net DLLs used by the plugin. The source for the DLLs can be found at https://bitbucket.org/cythereal/magic-clients

[MAGIC Upload]: http://docs.cythereal.com/en/latest/src/usage_guide/upload.html
[MAGIC Correlations]: http://docs.cythereal.com/en/latest/src/usage_guide/magic_report.html#magic-correlations
[MAGIC Categories]: http://docs.cythereal.com/en/latest/src/usage_guide/magic_report.html#magic-categories
